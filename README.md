# tor-2e-compendium-ptbr
TOR2E - Compêndio não oficial - Versão brasileira

## Compatibilidade
Foundry VTT v11
            v12

## Conteúdo
Este compêndio fornece diversos itens necessários para a criação de personagens (PCs, NPCs, Adversários) no sistema O Um Anel 2e (https://foundryvtt.com/packages/tor2e):
- Características Notáveis, Virtudes, Falhas, Habilidades Mortais, Perícias e Recompensas
- Armas, Armaduras e escudos
- Adversários


## Todos esses itens são criados
- com suas estatísticas, se existirem
- sem nenhum texto ou imagem dos livros oficiais
- com uma referência de onde encontrar o texto e as imagens no Livro Básico

Todas as imagens usadas para os itens são imagens fornecidas pelo sistema TOR2E (Características Notáveis, Virtudes, Falhas, Habilidades Mortais e Recompensas) ou de VTTicons gratuitos do Foundry (armas, armaduras, escudos).

Nota: Não há Efeito Ativo implementado em nenhum item deste compêndio.


## Pronto para usar / Pronto para modificar
De acordo com o direito de propriedade e licença, e como este módulo do compêndio é público: ele não contém nenhuma descrição de texto, nenhum texto de regra, nenhuma imagem de qualquer um dos livros de RPG O Um Anel. Mas todos os itens, depois de serem criados em um jogo VTT no próprio Foundry do Mestre (Historiador), ficam em modo privado / não são compartilhados com o público, e então o Mestre (Historiador) pode modificá-los para adicionar qualquer descrição e imagem necessárias de sua própria cópia do Livro Básico.


## Como usar
Vá para a guia "Compêndio". Você encontrará os seguintes pacotes:
- tor-2e-compendium-ptbr: Equipamento
- tor-2e-compendium-ptbr: Características
- tor-2e-compendium-ptbr: Oponentes
Você pode também
- abrir o pacote, arrastar e soltar o conteúdo na aba direita (Objetos para Características e Equipamentos, Atores para Adversários)
- ou clicar com o botão direito nos pacotes e importar todo o conteúdo para o seu mundo

Documentação do Foundry VTT sobre compêndios: https://foundryvtt.com/article/compendium/
